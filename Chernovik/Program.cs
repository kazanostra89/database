﻿using System;


namespace Chernovik
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int n, m;
            n = 3;
            m = 3;

            int[,] mas = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    mas[i, j] = rnd.Next(50);
                }
            }

            PrintArray(mas);

            Console.ReadKey();
        }

        static void PrintArray(int[,] mas)
        {
            for (int i = 0; i < mas.GetLength(0); i++)
            {
                for (int j = 0; j < mas.GetLength(1); j++)
                {
                    Console.Write("{0,-5}", mas[i,j]);
                }
                Console.WriteLine();
            }
        }


        #region Old
        /*static void SortArray(int[] mas)
        {
            bool sort;
            int step = 0, bufer;

            do
            {
                sort = false;

                for (int i = 0; i < mas.Length - 1; i++)
                {
                    if (mas[i] > mas[i + 1])
                    {
                        bufer = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = bufer;

                        sort = true;
                    }
                }

                step++;

            } while (sort == true);
        }

        static Number[] NewArray(Number[] nas, int newN)
        {
            Number[] newMas = new Number[newN];

            if (newN > nas.Length)
            {
                for (int i = 0; i < nas.Length; i++)
                {
                    newMas[i] = nas[i];
                }
            }
            else
            {
                for (int i = 0; i < newN; i++)
                {
                    newMas[i] = nas[i];
                }
            }
            return newMas;

        }

        static void NewArrayTwo(ref Number[] nas, int newN)
        {
            Number[] newMas = new Number[newN];

            if (newN > nas.Length)
            {
                for (int i = 0; i < nas.Length; i++)
                {
                    newMas[i] = nas[i];
                }
            }
            else
            {
                for (int i = 0; i < newN; i++)
                {
                    newMas[i] = nas[i];
                }
            }
            nas = newMas;

        }

        static int Min(int[] mas)
        {
            int min = mas[0];
            for (int i = 0; i < mas.Length; i++)
            {
                if (min > mas[i])
                {
                    min = mas[i];
                }
            }
            return min;
        }*/
        #endregion

    }
}
