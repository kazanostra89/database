﻿using System;
using System.IO;

namespace FurnitureStoreDz
{
    class Program
    {
        enum ClassFurniture
        {
            Economy = 1,
            Standart,
            Luxury,
            Premium
        }

        struct StoreFurniture
        {
            public static int CURRENT_ID = 0;

            public int Id;
            public string Name;
            public float Weight;
            public ClassFurniture ClassF;
            public float RatingCustomer;
        }

        #region Ввод значений переменных и полей структур
        static int InputInt(string message)
        {
            bool check;
            int number;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        static bool InputBool(string message)
        {
            bool check;
            bool input;

            do
            {
                Console.Write(message);
                check = bool.TryParse(Console.ReadLine(), out input);

            } while (check == false);

            return input;
        }

        static string InputString(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        static float InputFloat(string message, bool rating)
        {
            bool check;
            float number;

            if (rating == true)
            {
                do
                {
                    Console.Write(message);
                    check = float.TryParse(Console.ReadLine(), out number);

                } while (check == false || number < 0 || number > 5.0);
            }
            else
            {
                do
                {
                    Console.Write(message);
                    check = float.TryParse(Console.ReadLine(), out number);

                } while (check == false);
            }

            return number;
        }

        static ClassFurniture InputClassF(string message)
        {
            bool check;
            ClassFurniture number;

            do
            {
                Console.Write(message);
                check = ClassFurniture.TryParse(Console.ReadLine(), out number);

            } while (check == false || number < ClassFurniture.Economy || number > ClassFurniture.Premium);

            return number;
        }
        #endregion

        #region Печать массива
        static void PrintManyFurnitures(StoreFurniture[] furnitures)
        {
            Console.WriteLine("{0,-3}{1,-15}{2,-6}{3,-9}{4,-8}", "ID", "Наименование", "Вес", "Тип", "Рейтинг");

            if (furnitures == null)
            {
                Console.WriteLine("Массив пуст!");
            }
            else if (furnitures.Length == 0)
            {
                Console.WriteLine("Массив пуст!");
            }
            else
            {
                for (int i = 0; i < furnitures.Length; i++)
                {
                    PrintFurnitures(furnitures[i]);
                }
            }

            Console.WriteLine("----------------------------------------");
        }

        static void PrintFurnitures(StoreFurniture furniture)
        {
            Console.WriteLine("{0,-3}{1,-15}{2,-6}{3,-9}{4,-8}", furniture.Id, furniture.Name, furniture.Weight, furniture.ClassF, furniture.RatingCustomer);
        }
        #endregion

        static void Main(string[] args)
        {
            StoreFurniture[] furnitures = null;
            bool playProgram;
            int menuNumberInput;

            playProgram = true;

            while (playProgram)
            {
                Console.Clear();

                PrintManyFurnitures(furnitures);

                PrintMenu();
                menuNumberInput = InputInt("Введите номер соответствующий меню программы: ");

                switch (menuNumberInput)
                {
                    case 1:
                        {
                            StoreFurniture furniture = CreateFurniture();
                            AddNewFurnitures(ref furnitures, furniture);
                        }
                        break;
                    case 2:
                        {
                            bool ascVar = InputBool("Выберите сортировку: возрастание(true)/убывание(false)--> ");
                            SortingRatingCustomer(furnitures, ascVar);
                        }
                        break;
                    case 3:
                        {
                            Console.WriteLine($"Минимальный вес равен {GetMinWeight(furnitures)} максимальный вес равен {GetMaxWeight(furnitures)}.");

                            float minWeight = InputFloat("Введите минимальное значение веса: ", false);
                            float maxWeight = InputFloat("Введите максимальное значение веса: ", false);

                            StoreFurniture[] newFurnituresFinded = FindFurnituresFromMinToMaxWeights(furnitures, minWeight, maxWeight);
                            PrintManyFurnitures(newFurnituresFinded);
                        }
                        break;
                    case 4:
                        {
                            string fileName = InputString("Введите имя файла: ");
                            PrintManyFurnituresToFile(furnitures, fileName);
                        }
                        break;


                    case 0:
                        {
                            Console.WriteLine("Выход из программы");
                            playProgram = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Введенный номер отсутствует в меню...");
                        }
                        break;
                }

                Console.ReadKey();
            }
            
        }

        static void PrintMenu()
        {
            Console.WriteLine("1. Добавление мебели");
            Console.WriteLine("2. Сортировка по потребительскому рейтингу");
            Console.WriteLine("3. Вывод мебели в диапазоне от min до max веса");
            Console.WriteLine("4. Вывод списка мебели в файл");
            Console.WriteLine("0. Выход из программы...");
        }

        static StoreFurniture CreateFurniture()
        {
            StoreFurniture furniture;

            StoreFurniture.CURRENT_ID++;

            furniture.Id = StoreFurniture.CURRENT_ID;

            furniture.Name = InputString("Укажите имя мебели: ");
            furniture.Weight = InputFloat("Укажите вес мебели: ", false);
            furniture.ClassF = InputClassF("Укажите тип мебели (Economy, Standart, Luxury, Premium): ");
            furniture.RatingCustomer = InputFloat("Укажите потребительский рейтинг мебели (0 до 5.0): ", true);


            return furniture;
        }

        static void AddNewFurnitures(ref StoreFurniture[] furnitures, StoreFurniture furniture)
        {
            if (furnitures == null)
            {
                furnitures = new StoreFurniture[1];
            }
            else
            {
                ResizeArray(ref furnitures);
            }

            furnitures[furnitures.Length - 1] = furniture;
        }

        static void ResizeArray(ref StoreFurniture[] furnitures)
        {
            StoreFurniture[] newFurnitures = new StoreFurniture[furnitures.Length + 1];

            if (newFurnitures.Length > furnitures.Length)
            {
                for (int i = 0; i < furnitures.Length; i++)
                {
                    newFurnitures[i] = furnitures[i];
                }
            }
            else
            {
                for (int i = 0; i < newFurnitures.Length; i++)
                {
                    newFurnitures[i] = furnitures[i];
                }
            }

            furnitures = newFurnitures;
        }

        static void SortingRatingCustomer(StoreFurniture[] furnitures, bool ascVar)
        {
            int stepSorting;
            bool sorting, selectionSorting;
            StoreFurniture bufer;

            stepSorting = 0;

            if (furnitures == null)
            {
                Console.WriteLine("Массив пуст, сортировка невозможна!");
            }
            else if (furnitures.Length == 0)
            {
                Console.WriteLine("Массив пуст, сортировка невозможна!");
            }
            else
            {
                do
                {
                    sorting = false;

                    for (int i = 0; i < furnitures.Length - 1 - stepSorting; i++)
                    {
                        if (ascVar)
                        {
                            selectionSorting = furnitures[i].RatingCustomer > furnitures[i + 1].RatingCustomer;
                        }
                        else
                        {
                            selectionSorting = furnitures[i].RatingCustomer < furnitures[i + 1].RatingCustomer;
                        }

                        if (selectionSorting)
                        {
                            bufer = furnitures[i];
                            furnitures[i] = furnitures[i + 1];
                            furnitures[i + 1] = bufer;

                            sorting = true;
                        }
                    }

                    stepSorting++;

                } while (sorting);
            }

        }

        static float GetMinWeight(StoreFurniture[] furnitures)
        {
            float minFurniture = furnitures[0].Weight;

            for (int i = 0; i < furnitures.Length; i++)
            {
                if (minFurniture > furnitures[i].Weight)
                {
                    minFurniture = furnitures[i].Weight;
                }
            }

            return minFurniture;
        }

        static float GetMaxWeight(StoreFurniture[] furnitures)
        {
            float maxFurniture = furnitures[0].Weight;

            for (int i = 0; i < furnitures.Length; i++)
            {
                if (maxFurniture < furnitures[i].Weight)
                {
                    maxFurniture = furnitures[i].Weight;
                }
            }

            return maxFurniture;
        }

        static StoreFurniture[] FindFurnituresFromMinToMaxWeights(StoreFurniture[] furnitures, float minWeight, float maxWeight)
        {
            StoreFurniture[] printToMaxMin = null;

            for (int i = 0; i < furnitures.Length; i++)
            {
                if (furnitures[i].Weight >= minWeight && furnitures[i].Weight <= maxWeight)
                {
                    AddNewFurnitures(ref printToMaxMin, furnitures[i]);
                }
            }

            return printToMaxMin;
        }

        static void PrintManyFurnituresToFile(StoreFurniture[] furnitures, string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);

            writer.WriteLine("{0,-3}{1,-15}{2,-6}{3,-9}{4,-8}", "ID", "Наименование", "Вес", "Тип", "Рейтинг");

            try
            {
                if (furnitures == null)
                {
                    writer.WriteLine("Массив пуст!");
                }
                else if (furnitures.Length == 0)
                {
                    writer.WriteLine("Массив пуст!");
                }
                else
                {
                    for (int i = 0; i < furnitures.Length; i++)
                    {
                        writer.WriteLine("{0,-3}{1,-15}{2,-6}{3,-9}{4,-8}", furnitures[i].Id, furnitures[i].Name, furnitures[i].Weight, furnitures[i].ClassF, furnitures[i].RatingCustomer);
                    }
                }

                Console.WriteLine("Запись выполнена!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            writer.Close();
        }

    }
}
