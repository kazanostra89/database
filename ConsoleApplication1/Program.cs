﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ProductShop
{
    class Program
    {
        
        [Serializable]
        struct Product
        {
            public static int CURRENT_ID = 0;

            public int Id;                  // Уникальный ключ, автозаполняемый
            public string Name;             // имя продукта
            public string Contractor;       // поставщик
            public DateTime DeliveryDate;   // дата поставки
            public int SelfLifeDays;        // срок годности
            public int Balance;             // всего продуктов
        }

        [Serializable]
        struct ProductsList
        {
            public int CURRENT_ID;
            public Product[] products;
        }

        #region System Methods
        static int InputInt(string message)
        {
            bool inputResult;
            int number;

            do
            {
                Console.Write(message);
                inputResult = int.TryParse(Console.ReadLine(), out number);

            } while (!inputResult || number < 0);

            return number;
        }

        static DateTime InputDateTime(string message)
        {
            bool inputResult;
            DateTime dt;

            do
            {
                Console.Write(message);
                inputResult = DateTime.TryParse(Console.ReadLine(), out dt);

            } while (!inputResult);

            return dt;
        }

        static bool InputBool(string message)
        {
            bool inputResult;
            bool b;

            do
            {
                Console.Write(message);
                inputResult = bool.TryParse(Console.ReadLine(), out b);

            } while (!inputResult);

            return b;
        }

        static string InputString(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }

        static void ResizeArray(ref Product[] products, int newLength)
        {
            //int minLength = newLength > products.Length ? products.Length : newLength;
            int minLength;

            if (newLength > products.Length)
            {
                minLength = products.Length;
            }
            else
            {
                minLength = newLength;
            }

            Product[] newArray = new Product[newLength];

            for (int i = 0; i < minLength; i++)
            {
                newArray[i] = products[i];
            }

            products = newArray;

        }
        #endregion

        #region CRUD Methonds

        static void AddNewProduct(ref Product[] products, Product product)
        {
            if (products == null)
            {
                products = new Product[1];
                //products[0] = product;
            }
            else
            {
                ResizeArray(ref products, products.Length + 1);
                //products[products.Length - 1] = product;
            }
            products[products.Length - 1] = product;
        }

        static void DeleteProductById(ref Product[] products, int id)
        {
            int indexDelete = GetIndexById(products, id);

            if (indexDelete == -1)
            {
                Console.WriteLine("Delete is impossible");
                return;
            }

            Product[] newProducts = new Product[products.Length - 1];
            int newI = 0;

            for (int i = 0; i < products.Length; i++)
            {
                if (i != indexDelete)
                {
                    newProducts[newI] = products[i];
                    newI++;
                }
            }

            products = newProducts;
        }

        static void ClearAllProducts(ref Product[] products)
        {
            products = null;
        }

        static void UpdateProductsById(Product[] products, Product product, int id)
        {
            int indexUpdate = GetIndexById(products, id);

            if (indexUpdate == -1)
            {
                Console.WriteLine("Update is impossible");
                return;
            }

            product.Id = products[indexUpdate].Id;
            products[indexUpdate] = product;

        }

        #endregion

        static void InsertProductIntoPosition(ref Product[] products, int position, Product product)
        {
            if (products == null)
            {
                Console.WriteLine("Insert is impossible. Array is empty");
                return;
            }

            if (position < 1 || position > products.Length)
            {
                Console.WriteLine("Insert is impossible. Position not found");
                return;
            }

            int indexInsert = position - 1;

            Product[] newProducts = new Product[products.Length + 1];
            int oldI = 0;

            for (int i = 0; i < newProducts.Length; i++)
            {
                if(i != indexInsert)
                {
                    newProducts[i] = products[oldI];
                    oldI++;
                }
                else
                {
                    newProducts[i] = product;
                }
            }

            products = newProducts;
        }

        #region Tools Methods

        static DateTime GetMinDeliveryDate(Product[] products)
        {
            DateTime minDate = products[0].DeliveryDate;

            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].DeliveryDate < minDate)
                {
                    minDate = products[i].DeliveryDate;
                }
            }

            return minDate;
        }

        static DateTime GetMaxDeliveryDate(Product[] products)
        {
            DateTime maxDate = products[0].DeliveryDate;

            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].DeliveryDate > maxDate)
                {
                    maxDate = products[i].DeliveryDate;
                }
            }

            return maxDate;
        }

        static int GetMinSelfLifeDays(Product[] products)
        {
            int minSelfLifeDays = products[0].SelfLifeDays;

            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].SelfLifeDays < minSelfLifeDays)
                {
                    minSelfLifeDays = products[i].SelfLifeDays;
                }
            }

            return minSelfLifeDays;
        }

        static int GetMaxSelfLifeDays(Product[] products)
        {
            int maxSelfLifeDays = products[0].SelfLifeDays;

            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].SelfLifeDays > maxSelfLifeDays)
                {
                    maxSelfLifeDays = products[i].SelfLifeDays;
                }
            }

            return maxSelfLifeDays;
        }

        static int GetIndexById(Product[] products ,int id)
        {
            if (products == null)
            {
                return -1;
            }

            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].Id == id)
                {
                    return i;
                }
            }

            return -1;
        }

        static Product CreateProduct(bool isNewId)
        {
            Product product;

            if (isNewId)
            {
                Product.CURRENT_ID++;
                product.Id = Product.CURRENT_ID;
            }
            else
            {
                product.Id = 0;
            }

            product.Name = InputString("Введите название продукта: ");
            product.Contractor = InputString("Введите поставщика: ");
            product.DeliveryDate = InputDateTime("Введите дату доставки: ");
            product.SelfLifeDays = InputInt("Введите срок годности: ");
            product.Balance = InputInt("Введите остаток продукта: ");

            return product;
        }

        static Product CreateEmptyProduct()
        {
            Product product;
            product.Id = 0;
            product.Name = "";
            product.Contractor = "";
            product.Balance = 0;
            product.SelfLifeDays = 0;
            product.DeliveryDate = DateTime.Now;
            return product;
        }

        static void PrintProduct(Product product)
        {
            Console.WriteLine("{0,-3}{1,-15}{2,-15}{3,-12}{4,-4}{5,-4}", product.Id, product.Name, product.Contractor, product.DeliveryDate.ToShortDateString(), product.SelfLifeDays, product.Balance);
        }

        static void PrintManyProducts(Product[] products)
        {
            Console.WriteLine("{0,-3}{1,-15}{2,-15}{3,-12}{4,-4}{5,-4}", "ИД", "Название", "Поставщик", "Дата дост.", "СГ", "Ост.");

            if (products == null)
            {
                Console.WriteLine("Arrays is empty");
            }
            else if (products.Length == 0)
            {
                Console.WriteLine("Arrays is empty");
            }
            else
            {
                for (int i = 0; i < products.Length; i++)
                {
                    PrintProduct(products[i]);
                }
            }
            Console.WriteLine("--------------------");
        }

        static void PrintManyProductsToFile(Product[] products, string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);

            writer.WriteLine("{0,-3}{1,-15}{2,-15}{3,-12}{4,-4}{5,-4}", "ИД", "Название", "Поставщик", "Дата дост.", "СГ", "Ост.");

            if (products == null)
            {
                writer.WriteLine("Arrays is empty");
            }
            else if (products.Length == 0)
            {
                writer.WriteLine("Arrays is empty");
            }
            else
            {
                for (int i = 0; i < products.Length; i++)
                {
                    writer.WriteLine("{0,-3}{1,-15}{2,-15}{3,-12}{4,-4}{5,-4}", products[i].Id, products[i].Name, products[i].Contractor, products[i].DeliveryDate.ToShortDateString(), products[i].SelfLifeDays, products[i].Balance);
                }
            }
            writer.Close();
        }

        static void SaveManyProductsToFile(Product[] products, string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);

            writer.WriteLine(products.Length);
            writer.WriteLine(Product.CURRENT_ID);

            for (int i = 0; i < products.Length; i++)
            {
                writer.WriteLine(products[i].Id);
                writer.WriteLine(products[i].Name);
                writer.WriteLine(products[i].Contractor);
                writer.WriteLine(products[i].DeliveryDate);
                writer.WriteLine(products[i].SelfLifeDays);
                writer.WriteLine(products[i].Balance);
            }
            writer.Close();
        }

        static Product[] LoadManyProductsFromFile(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);

            int countProducts = int.Parse(reader.ReadLine());
            Product.CURRENT_ID = int.Parse(reader.ReadLine());

            Product[] products = new Product[countProducts];

            for (int i = 0; i < products.Length; i++)
            {
                products[i].Id = int.Parse(reader.ReadLine());
                products[i].Name = reader.ReadLine();
                products[i].Contractor = reader.ReadLine();
                products[i].DeliveryDate = DateTime.Parse(reader.ReadLine());
                products[i].SelfLifeDays = int.Parse(reader.ReadLine());
                products[i].Balance = int.Parse(reader.ReadLine());
            }

            reader.Close();

            return products;
        }

        static void SerializManyProductsToFile(Product[] products, string fileName)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);

            ProductsList productsList;
            productsList.CURRENT_ID = Product.CURRENT_ID;
            productsList.products = products;

            formatter.Serialize(stream, productsList);

            stream.Close();
        }

        static Product[] DeserializManyProductsToFile(string fileName)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);

            ProductsList productsLists = (ProductsList) formatter.Deserialize(stream);

            stream.Close();

            Product.CURRENT_ID = productsLists.CURRENT_ID;

            return productsLists.products;
        }

        #endregion

        #region Interface Methods

        static void PrintMenu()
        {
            Console.WriteLine("1. Add new product");
            Console.WriteLine("2. Delete product by id");
            Console.WriteLine("3. Clear all products");
            Console.WriteLine("4. Update products by id");
            Console.WriteLine("5. Insert product into position");
            Console.WriteLine("6. Print product by id");
            Console.WriteLine("7. Find products from min to max delivery date");
            Console.WriteLine("8. Find products from min to max selflife days");
            Console.WriteLine("9. Sort products by balanse");
            Console.WriteLine("10. Sort products by Id");
            Console.WriteLine("11. Print products to txt file");
            Console.WriteLine("12. Save products to data file");
            Console.WriteLine("13. Load products from data file");
            Console.WriteLine("14. Serialize products from data file");
            Console.WriteLine("15. Deserialize products from data file");
            Console.WriteLine("0. Exit");
        }
        

        #endregion

        #region Retrive Methods

        static bool FindProductById(Product[] products, int id, out Product product)
        {
            int indexPrint = GetIndexById(products, id);

            if (indexPrint == -1)
            {
                //Console.WriteLine("Print is impossible");
                product = CreateEmptyProduct();
                return false;
            }
            else
            {
                product = products[indexPrint];
                return true;
            }

            //PrintProduct(products[indexPrint]);
        }

        static Product[] FindProductsFromMinToMaxDeliveryDate(Product[] products, DateTime minDate, DateTime maxDate)
        {
            Product[] findedProducts = null;

            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].DeliveryDate >= minDate && products[i].DeliveryDate <= maxDate)
                {
                    AddNewProduct(ref findedProducts, products[i]);
                }
            }

            return findedProducts;
        }

        static Product[] FindProductsFromMinToMaxSelfLifeDays(Product[] products, int minSelfLifeDays, int maxSelfLifeDays)
        {
            Product[] findedProducts = null;

            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].SelfLifeDays >= minSelfLifeDays && products[i].SelfLifeDays <= maxSelfLifeDays)
                {
                    AddNewProduct(ref findedProducts, products[i]);
                }
            }

            return findedProducts;
        }

        #endregion

        #region Sortirovka
        static void SortProductsByBalance(Product[] products, bool asc)
        {
            Product temp;
            bool sort;
            int offset = 0;

            do
            {
                sort = true;

                for (int i = 0; i < products.Length - 1 - offset; i++)
                {
                    bool compareResult;

                    if (asc)
                    {
                        compareResult = products[i + 1].Balance < products[i].Balance;
                    }
                    else
                    {
                        compareResult = products[i + 1].Balance > products[i].Balance;
                    }

                    if (compareResult)
                    {
                        temp = products[i];
                        products[i] = products[i + 1];
                        products[i + 1] = temp;

                        sort = false;
                    }
                }

                offset++;

            } while (sort == false);

        }

        static void SortProductsById(Product[] products, bool asc)
        {
            bool sort;
            Product bufer;
            int step = 0;

            do
            {
                sort = false;

                for (int i = 0; i < products.Length - 1 - step; i++)
                {
                    bool stepAsc;

                    if (asc)
                    {
                        stepAsc = products[i].Id > products[i + 1].Id;
                    }
                    else
                    {
                        stepAsc = products[i].Id < products[i + 1].Id;
                    }

                    if (stepAsc)
                    {
                        bufer = products[i];
                        products[i] = products[i + 1];
                        products[i + 1] = bufer;

                        sort = true;
                    }

                }

                step++;

            } while (sort == true);
        }
        #endregion

        static void Main(string[] args)
        {
            Product[] products = null;
            bool runProgram = true;

            while (runProgram)
            {
                Console.Clear();
                PrintManyProducts(products);

                PrintMenu();
                int menuPoint = InputInt("Input menu point: ");

                switch (menuPoint)
                {
                    case 1:
                        {
                            Product product = CreateProduct(true);
                            AddNewProduct(ref products, product);
                        }
                        break;
                    case 2:
                        {
                            int id = InputInt("Input id for delete: ");
                            DeleteProductById(ref products, id);
                        }
                        break;
                    case 3:
                        {
                            ClearAllProducts(ref products);
                        }
                        break;
                    case 4:
                        {
                            int id = InputInt("Input id for update: ");
                            Product product = CreateProduct(false);
                            UpdateProductsById(products, product, id);
                        }
                        break;
                    case 5:
                        {
                            int position = InputInt("Input position for insert");
                            Product product = CreateProduct(true);
                            InsertProductIntoPosition(ref products, position, product);
                        }
                        break;
                    case 6:
                        {
                            int id = InputInt("Input id forretrive: ");
                            Product product;
                            bool isFinded = FindProductById(products, id, out product);
                            if (isFinded)
                            {
                                PrintProduct(product);
                            }
                            else
                            {
                                Console.WriteLine("Print is impossible. Element not found");
                            }
                        }
                        break;
                    case 7:
                        {
                            Console.WriteLine($"Input min and max delivery date from {GetMinDeliveryDate(products).ToShortDateString()} to {GetMaxDeliveryDate(products).ToShortDateString()}");

                            DateTime minDate = InputDateTime("min date: ");
                            
                            DateTime maxDate = InputDateTime("max date: ");

                            Product[] findedProducts = FindProductsFromMinToMaxDeliveryDate(products, minDate, maxDate);
                            
                            PrintManyProducts(findedProducts);
                        }
                        break;
                    case 8:
                        {
                            Console.WriteLine($"Input min and max selflife days from {GetMinSelfLifeDays(products)} to {GetMaxSelfLifeDays(products)}");
                            
                            int minSelfLifeDays = InputInt("min selflife: ");
                            
                            int maxSelfLifeDays = InputInt("max selflife: ");

                            Product[] findedProducts = FindProductsFromMinToMaxSelfLifeDays(products, minSelfLifeDays, maxSelfLifeDays);

                            PrintManyProducts(findedProducts);
                        }
                        break;
                    case 9:
                        {
                            bool asc = InputBool("Input asc or desc sort Balance (true or false): ");
                            SortProductsByBalance(products, asc);
                        }
                        break;
                    case 10:
                        {
                            bool asc = InputBool("Input asc or desc sort Id (true or false): ");
                            SortProductsById(products, asc);
                        }
                        break;
                    case 11:
                        {
                            string fileName = InputString("Print file, name: ");
                            PrintManyProductsToFile(products, fileName);
                        }
                        break;
                    case 12:
                        {
                            string fileName = InputString("Save to file, name: ");
                            SaveManyProductsToFile(products, fileName);
                        }
                        break;
                    case 13:
                        {
                            string fileName = InputString("Load from file, name: ");
                            products = LoadManyProductsFromFile(fileName);
                        }
                        break;
                    case 14:
                        {
                            string fileName = InputString("InputSe file name: ");
                            SerializManyProductsToFile(products, fileName);
                        }
                        break;
                    case 15:
                        {
                            string fileName = InputString("InputDese file name: ");
                            products = DeserializManyProductsToFile(fileName);
                        }
                        break;


                    case 0:
                        {
                            Console.WriteLine("Program will be finish");
                            runProgram = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Unknown command");
                        }
                        break;
                }

                Console.ReadKey();
            }
            

        }
    }
}
