﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBase
{
    class Program
    {
        static int DROWING_ID = 0;

        struct ArchiveKrany
        {
            public int IdDrowing;
            public string Format;
            public string Designation;
            public string Name;
            public int NumberKran;
        }

        static void Main(string[] args)
        {
            ArchiveKrany[] drowings = null;

            bool runProgram = true;

            while (runProgram)
            {
                Console.Clear();

                PrintManyDrowings(drowings);

                PrinMenu();
                int menuPoint = InputInt("Input menu point: ");

                switch (menuPoint)
                {
                    case 1:
                        {
                            ArchiveKrany drowing = CreateDrowing(true);
                            AddNewDrowing(ref drowings, drowing);
                        }
                        break;
                    case 2:
                        {
                            int idDrowing = InputInt("Input id for delete: ");
                            DeleteDrowingById(ref drowings, idDrowing);
                        }
                        break;
                    case 3:
                        {
                            ClearAllDrowing(ref drowings);
                        }
                        break;
                    case 4:
                        {
                            int idDrowing = InputInt("Input id for update: ");
                            ArchiveKrany drowing = CreateDrowing(false);
                            UpdateDrowing(drowings, idDrowing, drowing);
                        }
                        break;
                    case 5:
                        {
                            int position = InputInt("Input position for insert: ");
                            ArchiveKrany drowing = CreateDrowing(true);
                            InsertDrowingIntoPositoin(ref drowings, position, drowing);
                        }
                        break;
                    case 6:
                        {
                            int idDrowing = InputInt("Input id for retrive: ");
                            ArchiveKrany drowing;
                            bool isFinded = FindDrowingById(drowings, idDrowing, out drowing);

                            if (isFinded)
                            {
                                PrintDrowing(drowing);
                            }
                            else
                            {
                                Console.WriteLine("Print is impossible. Element not found!");
                            }
                        }
                        break;
                    case 7:
                        {
                            Console.WriteLine($"Input min and max selflife days from {GetMinIdDrowing(drowings)} to {GetMaxIdDrowing(drowings)}");

                            int minIdDrowing = InputInt("min IdDrowing: ");

                            int maxIdDrowing = InputInt("max IdDrowing: ");

                            ArchiveKrany[] findedArchiveKrany = FindArchiveKranyFromMinToMaxIdDrowing(drowings, minIdDrowing, maxIdDrowing);

                            PrintManyDrowings(findedArchiveKrany);
                        }
                        break;
                    case 8:
                        {
                            bool asc = InputBool("Input asc or desc sort (true or false): ");
                            SortProductsByBalance(drowings, asc);
                        }
                        break;

                    case 0:
                        {
                            Console.WriteLine("Program well be finish");
                            runProgram = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Unknown command!");
                        }
                        break;
                }

                Console.ReadKey();
            }

            Console.ReadKey();
        }

        #region Interface Methods
        static void PrinMenu()
        {
            Console.WriteLine("1. Add new drowing");
            Console.WriteLine("2. Delete drowing by Id");
            Console.WriteLine("3. Clear all drowings");
            Console.WriteLine("4. Update drowing by Id");
            Console.WriteLine("5. Insert drowing into position");
            Console.WriteLine("6. Print drowing by Id");
            Console.WriteLine("7. Find drowings from min to max id drowing");
            Console.WriteLine("8. Sort products by IdDrowing");
            Console.WriteLine("0. Exit");
        }

        static int InputInt(string message)
        {
            bool inputCheck;
            int number;

            do
            {
                Console.Write(message);
                inputCheck = int.TryParse(Console.ReadLine(), out number);

            } while (inputCheck == false);

            return number;
        }

        static bool InputBool(string message)
        {
            bool check;
            bool b;

            do
            {
                Console.Write(message);
                check = bool.TryParse(Console.ReadLine(), out b);

            } while (check == false);

            return b;
        }

        static string InputString(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }
        #endregion

        static ArchiveKrany CreateDrowing(bool isNewId)
        {
            ArchiveKrany drowing;
            if (isNewId)
            {
                DROWING_ID++;
                drowing.IdDrowing = DROWING_ID;
            }
            else
            {
                drowing.IdDrowing = 0;
            }
            
            drowing.Format = InputString("Введите формат чертежа: ");
            drowing.Designation = InputString("Введите обозначение чертежа: ");
            drowing.Name = InputString("Введите наименование чертежа: ");
            drowing.NumberKran = InputInt("Введите номер крана: ");

            return drowing;
        }

        static ArchiveKrany CreateEmptyProduct()
        {
            ArchiveKrany drowing;

            drowing.IdDrowing = 0;
            drowing.Format = "";
            drowing.Designation = "";
            drowing.Name = "";
            drowing.NumberKran = 0;

            return drowing;
        }

        static void AddNewDrowing(ref ArchiveKrany[] drowings, ArchiveKrany drowing)
        {            
            if (drowings == null)
            {
                drowings = new ArchiveKrany[1];
            }
            else
            {
                ResizeArray(ref drowings, drowings.Length + 1);
            }

            drowings[drowings.Length - 1] = drowing;
        }

        static void ResizeArray(ref ArchiveKrany[] drowings, int newLength)
        {
            ArchiveKrany[] newDrowings = new ArchiveKrany[newLength];

            if (newLength > drowings.Length)
            {
                for (int i = 0; i < drowings.Length; i++)
                {
                    newDrowings[i] = drowings[i];
                }
            }
            else
            {
                for (int i = 0; i < newLength; i++)
                {
                    newDrowings[i] = drowings[i];
                }
            }

            drowings = newDrowings;
        }

        static void PrintDrowing(ArchiveKrany drowing)
        {
            Console.WriteLine("{0,-3}{1,-8}{2,-15}{3,-15}{4,-12}", drowing.IdDrowing, drowing.Format, drowing.Designation, drowing.Name, drowing.NumberKran);
        }

        static void PrintManyDrowings(ArchiveKrany[] drowings)
        {
            Console.WriteLine("{0,-3}{1,-8}{2,-15}{3,-15}{4,-12}", "ИД", "Формат", "Обозначение", "Наименование", "Номер крана");

            if (drowings == null)
            {
                Console.WriteLine("Array is empty!");
            }
            else if (drowings.Length == 0)
            {
                Console.WriteLine("Array is empty!");
            }
            else
            {
                for (int i = 0; i < drowings.Length; i++)
                {
                    PrintDrowing(drowings[i]);
                }
            }

            Console.WriteLine("---------------------------------------------------------------");
        }

        static int GetIndexById(ArchiveKrany[] drowings, int idDrowing)
        {
            if (drowings == null)
            {
                return -1;
            }

            for (int i = 0; i < drowings.Length; i++)
            {
                if (drowings[i].IdDrowing == idDrowing)
                {
                    return i;
                }
            }

            return -1;
        }

        static void DeleteDrowingById(ref ArchiveKrany[] drowings, int idDrowing)
        {
            int indexDelete;
            indexDelete = GetIndexById(drowings, idDrowing);

            if (indexDelete == -1)
            {
                Console.WriteLine("Delete is impossible. Element not found!");
                return;
            }

            ArchiveKrany[] newDrowings = new ArchiveKrany[drowings.Length - 1];
            int newI = 0; //Индекс для нового массива

            for (int i = 0; i < drowings.Length; i++)
            {
                if (i != indexDelete)
                {
                    newDrowings[newI] = drowings[i];
                    newI++;
                }
            }

            drowings = newDrowings;
        }

        static void ClearAllDrowing(ref ArchiveKrany[] drowings)
        {
            drowings = null;
        }

        static void UpdateDrowing(ArchiveKrany[] drowings, int idDrowing, ArchiveKrany drowing)
        {
            int indexUpdate;
            indexUpdate = GetIndexById(drowings, idDrowing);

            if (indexUpdate == -1)
            {
                Console.WriteLine("Update is impossible. Element not found!");
                return;
            }

            drowing.IdDrowing = drowings[indexUpdate].IdDrowing;
            drowings[indexUpdate] = drowing;
        }

        static void InsertDrowingIntoPositoin(ref ArchiveKrany[] drowings, int position, ArchiveKrany drowing)
        {
            if (drowings == null)
            {
                Console.WriteLine("Insert impossible. Array is empty");
                return;
            }

            if (position < 1 || position > drowings.Length)
            {
                Console.WriteLine("Insert impossible. Position not found");
                return;
            }

            int indexInsert = position - 1;

            ArchiveKrany[] newDrowings = new ArchiveKrany[drowings.Length + 1];
            int oldDrowingsI = 0;

            for (int i = 0; i < newDrowings.Length; i++)
            {
                if (i != indexInsert)
                {
                    newDrowings[i] = drowings[oldDrowingsI];
                    oldDrowingsI++;
                }
                else
                {
                    newDrowings[indexInsert] = drowing;
                }
            }

            drowings = newDrowings;
        }

        static bool FindDrowingById(ArchiveKrany[] drowings, int idDrowing, out ArchiveKrany drowing)
        {
            int indexPrint;
            indexPrint = GetIndexById(drowings, idDrowing);

            if (indexPrint == -1)
            {
                drowing = CreateEmptyProduct();
                return false;
            }
            else
            {
                drowing = drowings[indexPrint];
                return true;
            }
        }

        static int GetMinIdDrowing(ArchiveKrany[] drowings)
        {
            int minIdDrowing = drowings[0].IdDrowing;

            for (int i = 0; i < drowings.Length; i++)
            {
                if (minIdDrowing > drowings[i].IdDrowing)
                {
                    minIdDrowing = drowings[i].IdDrowing;
                }
            }

            return minIdDrowing;
        }

        static int GetMaxIdDrowing(ArchiveKrany[] drowings)
        {
            int maxIdDrowing = drowings[0].IdDrowing;

            for (int i = 0; i < drowings.Length; i++)
            {
                if (maxIdDrowing < drowings[i].IdDrowing)
                {
                    maxIdDrowing = drowings[i].IdDrowing;
                }
            }

            return maxIdDrowing;
        }

        static ArchiveKrany[] FindArchiveKranyFromMinToMaxIdDrowing(ArchiveKrany[] drowings, int MinIdDrowing, int MaxIdDrowing)
        {
            ArchiveKrany[] findedArchiveKrany = null;

            for (int i = 0; i < drowings.Length; i++)
            {
                if (drowings[i].IdDrowing >= MinIdDrowing && drowings[i].IdDrowing <= MaxIdDrowing)
                {
                    AddNewDrowing(ref findedArchiveKrany, drowings[i]);
                }
            }

            return findedArchiveKrany;
        }

        static void SortProductsByBalance(ArchiveKrany[] drowings, bool asc)
        {
            ArchiveKrany bufer;
            bool sort;
            int step = 0;

            do
            {
                sort = false;

                for (int i = 0; i < drowings.Length - 1 - step; i++)
                {
                    bool compareResult;

                    if (asc)
                    {
                        compareResult = drowings[i + 1].IdDrowing < drowings[i].IdDrowing;
                    }
                    else
                    {
                        compareResult = drowings[i + 1].IdDrowing > drowings[i].IdDrowing;
                    }

                    if (compareResult)
                    {
                        bufer = drowings[i];
                        drowings[i] = drowings[i + 1];
                        drowings[i + 1] = bufer;

                        sort = true;
                    }
                }

                step++;

            } while (sort == true);

        }

    }
}
